import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {
  opcion: number=0; 

  func1ofB1() {console.log("aun no se presiono el boton 1");}
  func2ofB1() {
    console.log("se presiono el boton 1")
    this.opcion=1;
  }
  
  func1ofB2() {console.log("aun no se presiono el boton 2");}
  func2ofB2() {
    console.log("se presiono el boton 2")
    this.opcion=2;
  }
  
  func1ofB3() {console.log("aun no se presiono el boton 3");}
  func2ofB3() {
    console.log("se presiono el boton 3")
    this.opcion=3;
  }
  
  constructor() { 
    this.func1ofB1()
    this.func1ofB2()
    this.func1ofB3()
  }

  ngOnInit(): void {
  }

}
